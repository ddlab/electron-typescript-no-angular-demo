import { app, BrowserWindow } from "electron";
// import {splash} from "@trodi/electron-splashscreen";
// import * as Splashscreen from "@trodi/electron-splashscreen";

import * as path from "path";

/*const winOpts = {
  title: "WIN_TITLE",
  icon: path.join(__dirname, '../assets/images/a.png'),
  frame: true,
  show: false,
  // backgroundColor: bgColor,
  center: true,
  autoHideMenuBar: false,
  menuBarVisibility: false,
  webPreferences: {
    nativeWindowOpen: true,
    nodeIntegrationInWorker: true
  }
}*/

function createWindow() {
  // With splash screen
  /*const mainWindow = Splashscreen.initSplashScreen({
		windowOpts: winOpts,
		templateUrl: path.join(__dirname, '../assets/images/', 'a.png'),
		delay: 0,
		minVisible: 800,
		splashScreenOpts: {
			width: 425,
			height: 325,
			transparent: true
		}
	});*/
	
	
  // Create the browser window.
   const mainWindow = new BrowserWindow({
     height: 600,
     // webPreferences: {
     //   preload: path.join(__dirname, "preload.js"),
     // },
     width: 800,
   });

  // and load the index.html of the app.
  mainWindow.loadFile(path.join(__dirname, "../index.html"));

  //mainWindow.loadFile(path.join(__dirname, "index.html"));

  // Open the DevTools.
  // mainWindow.webContents.openDevTools();
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", () => {
  createWindow();

  app.on("activate", () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  });
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

// In this file you can include the rest of your app"s specific main process
// code. You can also put them in separate files and require them here.
